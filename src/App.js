import React, { Component } from 'react'
import Draggable from 'react-draggable'

const defaults = {
  xl: 400,
  yl: 400,
  rl: 100,
  xm: 500,
  ym: 200,
  rm: 50,
  xs: 300,
  ys: 200,
  rs: 20,
}

const calcTangentCoordinates = ({cx, cy, r, px, py}) => {
  // http://www.ambrsoft.com/TrigoCalc/Circles2/CirclePoint/CirclePointDistance.htm
  const rsq = Math.pow(r, 2)
  const xpasq = Math.pow(px-cx, 2)
  const ypbsq = Math.pow(py-cy, 2)
  const x1 = ((rsq*(px-cx)+r*(py-cy)*Math.sqrt(xpasq+ypbsq-rsq))/(xpasq + ypbsq)) + cx
  const y1 = ((rsq*(py-cy)-r*(px-cx)*Math.sqrt(xpasq+ypbsq-rsq))/(xpasq + ypbsq)) + cy
  const x2 = ((rsq*(px-cx)-r*(py-cy)*Math.sqrt(xpasq+ypbsq-rsq))/(xpasq + ypbsq)) + cx
  const y2 = ((rsq*(py-cy)+r*(px-cx)*Math.sqrt(xpasq+ypbsq-rsq))/(xpasq + ypbsq)) + cy
  return { x1, y1, x2, y2 }
}

const calcHomotheticCenter = ({x1, y1, r1, x2, y2, r2}) => {
  // https://en.wikipedia.org/wiki/Homothetic_center#Computing_homothetic_centers
  let x, y
  x = -r2*x1/(r1-r2)
  x = x + r1*x2/(r1-r2)
  y = -r2*y1/(r1-r2)
  y = y + r1*y2/(r1-r2)
  return { x, y }
}

const getInfiniteLine = ({x1, y1, x2, y2}) => {
  // https://www.mathsisfun.com/algebra/line-equation-2points.html
  if (y1 === y2) {
    // straight horizontal line
    return {
      x1: 0,
      y1: y1,
      x2: 800,
      y2: y1,
    }
  }
  if (x1 === x2) {
    // straight vertical line
    return {
      x1: x1,
      y1: 0,
      x2: x1,
      y2: 600,
    }
  }
  const m = (y2 - y1) / (x2 - x1)
  const b = y1-m*x1
  return {
    x1: 0,
    y1: b,
    x2: 800,
    y2: 800*m+b,
  }
}

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { ...defaults }
  }
  handleDrag(event, data) {
    const which = data.node.className.baseVal.split(' ')[0]
    const updates = {}
    updates[`x${which}`] = data.x
    updates[`y${which}`] = data.y
    this.setState(updates)
  }
  render() {
    const s = this.state
    const pLS = calcHomotheticCenter({ x1: s.xl, y1: s.yl, r1: s.rl, x2: s.xs, y2: s.ys, r2: s.rs })
    const pLM = calcHomotheticCenter({ x1: s.xl, y1: s.yl, r1: s.rl, x2: s.xm, y2: s.ym, r2: s.rm })
    const pMS = calcHomotheticCenter({ x1: s.xm, y1: s.ym, r1: s.rm, x2: s.xs, y2: s.ys, r2: s.rs })
    const infLine = getInfiniteLine({ x1: pLS.x, y1: pLS.y, x2: pMS.x, y2: pMS.y })
    const tangentsLS = calcTangentCoordinates({ cx: s.xl, cy: s.yl, r: s.rl, px: pLS.x, py: pLS.y })
    const tangentsLM = calcTangentCoordinates({ cx: s.xl, cy: s.yl, r: s.rl, px: pLM.x, py: pLM.y })
    const tangentsMS = calcTangentCoordinates({ cx: s.xm, cy: s.ym, r: s.rm, px: pMS.x, py: pMS.y })
    const handleDrag = this.handleDrag.bind(this)
    return (
      <div className="App">
        <div className="play-area">
          <svg viewBox="0 0 800 600">
            <circle
              cx={pLS.x}
              cy={pLS.y}
              r={5}
            />
            <circle
              cx={pLM.x}
              cy={pLM.y}
              r={5}
            />
            <circle
              cx={pMS.x}
              cy={pMS.y}
              r={5}
            />
            <line
              stroke="red"
              {...infLine}
            />
            <line
              stroke="gray"
              x1={tangentsLS.x1}
              y1={tangentsLS.y1}
              x2={pLS.x}
              y2={pLS.y}
            />
            <line
              stroke="gray"
              x1={tangentsLS.x2}
              y1={tangentsLS.y2}
              x2={pLS.x}
              y2={pLS.y}
            />
            <line
              stroke="gray"
              x1={tangentsLM.x1}
              y1={tangentsLM.y1}
              x2={pLM.x}
              y2={pLM.y}
            />
            <line
              stroke="gray"
              x1={tangentsLM.x2}
              y1={tangentsLM.y2}
              x2={pLM.x}
              y2={pLM.y}
            />
            <line
              stroke="gray"
              x1={tangentsMS.x1}
              y1={tangentsMS.y1}
              x2={pMS.x}
              y2={pMS.y}
            />
            <line
              stroke="gray"
              x1={tangentsMS.x2}
              y1={tangentsMS.y2}
              x2={pMS.x}
              y2={pMS.y}
            />
            <Draggable
              onDrag={handleDrag}
              defaultPosition={{x: defaults.xs, y: defaults.ys}}
            >
              <circle className="s" cx="0" cy="0" r={s.rs} />
            </Draggable>
            <Draggable
              onDrag={handleDrag}
              defaultPosition={{x: defaults.xm, y: defaults.ym}}
            >
              <circle className="m" cx="0" cy="0" r={s.rm} />
            </Draggable>
            <Draggable
              onDrag={handleDrag}
              defaultPosition={{x: defaults.xl, y: defaults.yl}}
            >
              <circle className="l" cx="0" cy="0" r={s.rl} />
            </Draggable>
          </svg>
        </div>
        <p className="explanation">
          After seeing the <a href="https://hooktube.com/watch?v=lubGnk0UZt0" target="_blank" rel="noreferrer noopener">Numberphile episode on balls and cones</a> I had to make this.
          <br />
          <a href="https://gitlab.com/kabo/balls-and-cones" target="_blank" rel="noreferrer noopener">Source</a>
        </p>
      </div>
    )
  }
}

export default App
